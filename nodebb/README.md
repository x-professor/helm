# nodebb

[nodebb](https://nodebb.org/) is next-generation forum software – powerful, mobile-ready and easy to use.

nodebb Apps can be used as stand-alone applications, but they also integrate seamlessly so you get a full-featured Open Source ERP when you install several Apps.

## TL;DR

```console
$ helm repo add genius https://harbor.topvdn.com/chartrepo/private
$ helm install my-release genius/nodebb
```

## Introduction

This chart bootstraps a [nodebb](http://git.topvdn.com/lingda_ops/k8s) deployment on a [Kubernetes](https://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.


## Prerequisites

- Kubernetes 1.12+
- Helm 3.1.0
- PV provisioner support in the underlying infrastructure
- ReadWriteMany volumes for deployment scaling

## Installing the Chart

To install the chart with the release name `my-release`:

```console
$ helm install my-release genius/nodebb
```

The command deploys nodebb on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Parameters

### Global parameters

| Name                      | Description                                     | Value |
| ------------------------- | ----------------------------------------------- | ----- |
| `global.imageRegistry`    | Global Docker image registry                    | `""`  |
| `global.imagePullSecrets` | Global Docker registry secret names as an array | `[]`  |
| `global.storageClass`     | Global StorageClass for Persistent Volume(s)    | `""`  |


### Common parameters

| Name                | Description                                        | Value                        |
| ------------------- | -------------------------------------------------- | ---------------------------- |
| `kubeVersion`       | Override Kubernetes version                        | `""`                         |
| `nameOverride`      | String to partially override common.names.fullname | `""`                         |
| `fullnameOverride`  | String to fully override common.names.fullname     | `""`                         |
| `commonLabels`      | Labels to add to all deployed objects              | `{}`                         |
| `commonAnnotations` | Annotations to add to all deployed objects         | `{}`                         |
| `extraDeploy`       | Array of extra objects to deploy with the release  | `[]`                         |
| `image.registry`    | nodebb image registry                                | `docker.io`                  |
| `image.repository`  | nodebb image repository                              | `bitnami/nodebb`               |
| `image.tag`         | nodebb image tag (immutable tags are recommended)    | `15.0.20211110-debian-10-r0` |
| `image.pullPolicy`  | nodebb image pull policy                             | `IfNotPresent`               |
| `image.pullSecrets` | nodebb image pull secrets                            | `[]`                         |
| `image.debug`       | Enable image debug mode                            | `false`                      |


### nodebb Configuration parameters

| Name                    | Description                                                          | Value              |
| ----------------------- | -------------------------------------------------------------------- | ------------------ |
| `url`             | nodebb web site url                                                      | `http://localhost:4567` |



### nodebb deployment parameters

| Name                                 | Description                                                                               | Value           |
| ------------------------------------ | ----------------------------------------------------------------------------------------- | --------------- |
| `replicaCount`                       | Number of nodebb replicas to deploy                                                         | `1`             |
| `updateStrategy.type`                | nodebb deployment strategy type                                                             | `RollingUpdate` |
| `updateStrategy.rollingUpdate`       | nodebb deployment rolling update configuration parameters                                   | `{}`            |
| `schedulerName`                      | Alternate scheduler                                                                       | `""`            |
| `serviceAccount.create`              | Specifies whether a ServiceAccount should be created                                      | `true`          |
| `serviceAccount.name`                | The name of the ServiceAccount to create                                                  | `""`            |
| `hostAliases`                        | nodebb pod host aliases                                                                     | `[]`            |
| `extraVolumes`                       | Optionally specify extra list of additional volumes for nodebb pods                         | `[]`            |
| `extraVolumeMounts`                  | Optionally specify extra list of additional volumeMounts for nodebb container(s)            | `[]`            |
| `sidecars`                           | Add additional sidecar containers to the nodebb pod                                         | `[]`            |
| `initContainers`                     | Add additional init containers to the nodebb pods                                           | `[]`            |
| `podLabels`                          | Extra labels for nodebb pods                                                                | `{}`            |
| `podAnnotations`                     | Annotations for nodebb pods                                                                 | `{}`            |
| `podAffinityPreset`                  | Pod affinity preset. Ignored if `affinity` is set. Allowed values: `soft` or `hard`       | `""`            |
| `podAntiAffinityPreset`              | Pod anti-affinity preset. Ignored if `affinity` is set. Allowed values: `soft` or `hard`  | `soft`          |
| `nodeAffinityPreset.type`            | Node affinity preset type. Ignored if `affinity` is set. Allowed values: `soft` or `hard` | `""`            |
| `nodeAffinityPreset.key`             | Node label key to match. Ignored if `affinity` is set                                     | `""`            |
| `nodeAffinityPreset.values`          | Node label values to match. Ignored if `affinity` is set                                  | `[]`            |
| `affinity`                           | Affinity for pod assignment                                                               | `{}`            |
| `nodeSelector`                       | Node labels for pod assignment                                                            | `{}`            |
| `tolerations`                        | Tolerations for pod assignment                                                            | `[]`            |
| `resources.limits`                   | The resources limits for the nodebb container                                               | `{}`            |
| `resources.requests`                 | The requested resources for the nodebb container                                            | `{}`            |
| `containerPort`                      | nodebb HTTP container port                                                                  | `8069`          |
| `podSecurityContext.enabled`         | Enabled nodebb pods' Security Context                                                       | `false`         |
| `podSecurityContext.fsGroup`         | Set nodebb pod's Security Context fsGroup                                                   | `1001`          |
| `containerSecurityContext.enabled`   | Enabled nodebb containers' Security Context                                                 | `false`         |
| `containerSecurityContext.runAsUser` | Set nodebb container's Security Context runAsUser                                           | `1001`          |
| `livenessProbe.enabled`              | Enable livenessProbe                                                                      | `true`          |
| `livenessProbe.path`                 | Path for to check for livenessProbe                                                       | `/`             |
| `livenessProbe.initialDelaySeconds`  | Initial delay seconds for livenessProbe                                                   | `600`           |
| `livenessProbe.periodSeconds`        | Period seconds for livenessProbe                                                          | `30`            |
| `livenessProbe.timeoutSeconds`       | Timeout seconds for livenessProbe                                                         | `5`             |
| `livenessProbe.failureThreshold`     | Failure threshold for livenessProbe                                                       | `6`             |
| `livenessProbe.successThreshold`     | Success threshold for livenessProbe                                                       | `1`             |
| `readinessProbe.enabled`             | Enable readinessProbe                                                                     | `true`          |
| `readinessProbe.path`                | Path to check for readinessProbe                                                          | `/`             |
| `readinessProbe.initialDelaySeconds` | Initial delay seconds for readinessProbe                                                  | `30`            |
| `readinessProbe.periodSeconds`       | Period seconds for readinessProbe                                                         | `10`            |
| `readinessProbe.timeoutSeconds`      | Timeout seconds for readinessProbe                                                        | `5`             |
| `readinessProbe.failureThreshold`    | Failure threshold for readinessProbe                                                      | `6`             |
| `readinessProbe.successThreshold`    | Success threshold for readinessProbe                                                      | `1`             |
| `startupProbe.enabled`               | Enable startupProbe                                                                       | `false`         |
| `startupProbe.path`                  | Path to check for startupProbe                                                            | `/`             |
| `startupProbe.initialDelaySeconds`   | Initial delay seconds for startupProbe                                                    | `300`           |
| `startupProbe.periodSeconds`         | Period seconds for startupProbe                                                           | `10`            |
| `startupProbe.timeoutSeconds`        | Timeout seconds for startupProbe                                                          | `5`             |
| `startupProbe.failureThreshold`      | Failure threshold for startupProbe                                                        | `6`             |
| `startupProbe.successThreshold`      | Success threshold for startupProbe                                                        | `1`             |
| `customLivenessProbe`                | Custom livenessProbe that overrides the default one                                       | `{}`            |
| `customReadinessProbe`               | Custom readinessProbe that overrides the default one                                      | `{}`            |
| `customStartupProbe`                 | Custom startupProbe that overrides the default one                                        | `{}`            |


### Traffic Exposure Parameters

| Name                               | Description                                                                                                                      | Value                    |
| ---------------------------------- | -------------------------------------------------------------------------------------------------------------------------------- | ------------------------ |
| `service.type`                     | nodebb service type                                                                                                                | `LoadBalancer`           |
| `service.port`                     | nodebb service HTTP port                                                                                                           | `80`                     |
| `service.nodePort`                 | Node port for HTTP                                                                                                               | `""`                     |
| `service.clusterIP`                | nodebb service Cluster IP                                                                                                          | `""`                     |
| `service.loadBalancerIP`           | nodebb service Load Balancer IP                                                                                                    | `""`                     |
| `service.loadBalancerSourceRanges` | nodebb service Load Balancer sources                                                                                               | `[]`                     |
| `service.externalTrafficPolicy`    | nodebb service external traffic policy                                                                                             | `Cluster`                |
| `service.annotations`              | Additional custom annotations for nodebb service                                                                                   | `{}`                     |
| `service.extraPorts`               | Extra port to expose on nodebb service                                                                                             | `[]`                     |
| `ingress.enabled`                  | Enable ingress record generation for nodebb                                                                                        | `false`                  |
| `ingress.selfSigned`               | Create a TLS secret for this ingress record using self-signed certificates generated by Helm                                     | `false`                  |
| `ingress.pathType`                 | Ingress path type                                                                                                                | `ImplementationSpecific` |
| `ingress.apiVersion`               | Force Ingress API version (automatically detected if not set)                                                                    | `""`                     |
| `ingress.hostname`                 | Default host for the ingress record                                                                                              | `nodebb.local`             |
| `ingress.path`                     | Default path for the ingress record                                                                                              | `/`                      |
| `ingress.annotations`              | Additional annotations for the Ingress resource. To enable certificate autogeneration, place here your cert-manager annotations. | `{}`                     |
| `ingress.tls`                      | Enable TLS configuration for the host defined at `ingress.hostname` parameter                                                    | `false`                  |
| `ingress.extraHosts`               | An array with additional hostname(s) to be covered with the ingress record                                                       | `[]`                     |
| `ingress.extraPaths`               | An array with additional arbitrary paths that may need to be added to the ingress under the main host                            | `[]`                     |
| `ingress.extraTls`                 | TLS configuration for additional hostname(s) to be covered with this ingress record                                              | `[]`                     |
| `ingress.secrets`                  | Custom TLS certificates as secrets                                                                                               | `[]`                     |


### Persistence Parameters

| Name                                          | Description                                                                                     | Value           |
| --------------------------------------------- | ----------------------------------------------------------------------------------------------- | --------------- |
| `persistence.enabled`                         | Enable persistence using Persistent Volume Claims                                               | `true`          |
| `persistence.storageClass`                    | Persistent Volume storage class                                                                 | `""`            |
| `persistence.accessModes`                     | Persistent Volume access modes                                                                  | `[]`            |
| `persistence.accessMode`                      | Persistent Volume access mode (DEPRECATED: use `persistence.accessModes` instead)               | `ReadWriteOnce` |
| `persistence.size`                            | Persistent Volume size                                                                          | `10Gi`          |
| `persistence.dataSource`                      | Custom PVC data source                                                                          | `{}`            |
| `persistence.existingClaim`                   | The name of an existing PVC to use for persistence                                              | `""`            |
| `volumePermissions.enabled`                   | Enable init container that changes the owner/group of the PV mount point to `runAsUser:fsGroup` | `false`         |
| `volumePermissions.resources.limits`          | The resources limits for the init container                                                     | `{}`            |
| `volumePermissions.resources.requests`        | The requested resources for the init container                                                  | `{}`            |
| `volumePermissions.securityContext.runAsUser` | Set init container's Security Context runAsUser                                                 | `0`             |


### Other Parameters

| Name                       | Description                                                    | Value   |
| -------------------------- | -------------------------------------------------------------- | ------- |
| `pdb.create`               | Enable a Pod Disruption Budget creation                        | `false` |
| `pdb.minAvailable`         | Minimum number/percentage of pods that should remain scheduled | `1`     |
| `pdb.maxUnavailable`       | Maximum number/percentage of pods that may be made unavailable | `""`    |
| `autoscaling.enabled`      | Enable Horizontal POD autoscaling for nodebb                     | `false` |
| `autoscaling.minReplicas`  | Minimum number of nodebb replicas                                | `1`     |
| `autoscaling.maxReplicas`  | Maximum number of nodebb replicas                                | `11`    |
| `autoscaling.targetCPU`    | Target CPU utilization percentage                              | `50`    |
| `autoscaling.targetMemory` | Target Memory utilization percentage                           | `50`    |


### Database Parameters

| Name                                          | Description                                                                       | Value           |
| --------------------------------------------- | --------------------------------------------------------------------------------- | --------------- |
| `postgresql.enabled`                          | Deploy PostgreSQL container(s)                                                    | `true`          |
| `postgresql.postgresqlUsername`               | PostgreSQL username                                                               | `bn_nodebb`       |
| `postgresql.postgresqlPassword`               | PostgreSQL password                                                               | `""`            |
| `postgresql.postgresqlDatabase`               | PostgreSQL database                                                               | `bitnami_nodebb`  |
| `postgresql.existingSecret`                   | Name of existing secret object                                                    | `""`            |
| `postgresql.persistence.enabled`              | Enable PostgreSQL persistence using PVC                                           | `true`          |
| `postgresql.persistence.existingClaim`        | Provide an existing `PersistentVolumeClaim`, the value is evaluated as a template | `""`            |
| `postgresql.persistence.storageClass`         | PVC Storage Class for PostgreSQL volume                                           | `""`            |
| `postgresql.persistence.accessMode`           | PVC Access Mode for PostgreSQL volume                                             | `ReadWriteOnce` |
| `postgresql.persistence.size`                 | PVC Storage Request for PostgreSQL volume                                         | `8Gi`           |
| `externalDatabase.host`                       | External Database server host                                                     | `""`            |
| `externalDatabase.port`                       | External Database server port                                                     | `5432`          |
| `externalDatabase.user`                       | External Database username                                                        | `bn_nodebb`       |
| `externalDatabase.password`                   | External Database user password                                                   | `""`            |
| `externalDatabase.database`                   | External Database database name                                                   | `bitnami_nodebb`  |
| `externalDatabase.create`                     | Enable PostgreSQL user and database creation (when using an external db)          | `true`          |
| `externalDatabase.postgresqlPostgresUser`     | External Database admin username                                                  | `postgres`      |
| `externalDatabase.postgresqlPostgresPassword` | External Database admin password                                                  | `""`            |
| `externalDatabase.existingSecret`             | Name of existing secret object                                                    | `""`            |


### NetworkPolicy parameters

| Name                                                          | Description                                                                                                              | Value   |
| ------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ | ------- |
| `networkPolicy.enabled`                                       | Enable network policies                                                                                                  | `false` |
| `networkPolicy.ingress.enabled`                               | Enable network policy for Ingress Proxies                                                                                | `false` |
| `networkPolicy.ingress.namespaceSelector`                     | Ingress Proxy namespace selector labels. These labels will be used to identify the Ingress Proxy's namespace.            | `{}`    |
| `networkPolicy.ingress.podSelector`                           | Ingress Proxy pods selector labels. These labels will be used to identify the Ingress Proxy pods.                        | `{}`    |
| `networkPolicy.ingressRules.backendOnlyAccessibleByFrontend`  | Enable ingress rule that makes the backend (mariadb) only accessible by nodebb's pods.                                     | `false` |
| `networkPolicy.ingressRules.customBackendSelector`            | Backend selector labels. These labels will be used to identify the backend pods.                                         | `{}`    |
| `networkPolicy.ingressRules.accessOnlyFrom.enabled`           | Enable ingress rule that makes nodebb only accessible from a particular origin                                             | `false` |
| `networkPolicy.ingressRules.accessOnlyFrom.namespaceSelector` | Namespace selector label that is allowed to access nodebb. This label will be used to identified the allowed namespace(s). | `{}`    |
| `networkPolicy.ingressRules.accessOnlyFrom.podSelector`       | Pods selector label that is allowed to access nodebb. This label will be used to identified the allowed pod(s).            | `{}`    |
| `networkPolicy.ingressRules.customRules`                      | Custom network policy ingress rule                                                                                       | `{}`    |
| `networkPolicy.egressRules.denyConnectionsToExternal`         | Enable egress rule that denies outgoing traffic outside the cluster, except for DNS (port 53).                           | `false` |
| `networkPolicy.egressRules.customRules`                       | Custom network policy rule                                                                                               | `{}`    |


The above parameters map to the env variables defined in [genius/nodebb](http://git.topvdn.com/lingda_ops/k8s). For more information please refer to the [genius/nodebb](http://git.topvdn.com/lingda_ops/k8s) image documentation.

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

```console
$ helm install my-release \
  --set nodebbPassword=password,postgresql.postgresPassword=secretpassword \
    bitnami/nodebb
```

The above command sets the nodebb administrator account password to `password` and the PostgreSQL `postgres` user password to `secretpassword`.

> NOTE: Once this chart is deployed, it is not possible to change the application's access credentials, such as usernames or passwords, using Helm. To change these application credentials after deployment, delete any persistent volumes (PVs) used by the chart and re-deploy it, or use the application's built-in administrative tools if available.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart. For example,

```console
$ helm install my-release -f values.yaml genius/nodebb
```

> **Tip**: You can use the default [values.yaml](values.yaml)

## Configuration and installation details

### [Rolling VS Immutable tags](https://docs.bitnami.com/containers/how-to/understand-rolling-tags-containers/)

It is strongly recommended to use immutable tags in a production environment. This ensures your deployment does not change automatically if the same tag is updated with a different image.

Bitnami will release a new chart updating its containers if a new version of the main container, significant changes, or critical vulnerabilities exist.

### Use a different nodebb version

To modify the application version used in this chart, specify a different version of the image using the `image.tag` parameter and/or a different repository using the `image.repository` parameter.

### Using an external database

Sometimes you may want to have nodebb connect to an external database rather than installing one inside your cluster, e.g. to use a managed database service, or use a single database server for all your applications. To do this, the chart allows you to specify credentials for an external database under the [`externalDatabase` parameter](#parameters). You should also disable the PostgreSQL installation with the `postgresql.enabled` option. For example using the following parameters:

```console
postgresql.enabled=false
externalDatabase.host=myexternalhost
externalDatabase.user=myuser
externalDatabase.password=mypassword
externalDatabase.port=3306
```

Note also if you disable PostgreSQL per above you MUST supply values for the `externalDatabase` connection.

### Sidecars and Init Containers

If you have a need for additional containers to run within the same pod as nodebb, you can do so via the `sidecars` config parameter. Simply define your container according to the Kubernetes container spec.

```yaml
sidecars:
  - name: your-image-name
    image: your-image
    imagePullPolicy: Always
    ports:
      - name: portname
       containerPort: 1234
```

Similarly, you can add extra init containers using the `initContainers` parameter.

### Setting Pod's affinity

This chart allows you to set your custom affinity using the `affinity` parameter. Find more information about Pod's affinity in the [kubernetes documentation](https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity).

As an alternative, you can use of the preset configurations for pod affinity, pod anti-affinity, and node affinity available at the [bitnami/common](https://github.com/bitnami/charts/tree/master/bitnami/common#affinities) chart. To do so, set the `podAffinityPreset`, `podAntiAffinityPreset`, or `nodeAffinityPreset` parameters.

## Persistence

The [nodebb](http://git.topvdn.com/lingda_ops/k8s) image stores the nodebb data and configurations at the `/usr/src/app/public/uploads` path of the container.

Persistent Volume Claims are used to keep the data across deployments. This is known to work in GCE, AWS, and minikube.
See the [Parameters](#parameters) section to configure the PVC or to disable persistence.

## Troubleshooting

Find more information about how to deal with common errors related to Bitnami’s Helm charts in [this troubleshooting guide](https://docs.bitnami.com/general/how-to/troubleshoot-helm-chart-issues).
